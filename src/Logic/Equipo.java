/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Logic;

/**
 *
 * @author josec
 */
public class Equipo {
    
    private String nombre_Equipo;
    private String dueno_Equipo;
    private String pais;
    private String liga_inscrita;

    public String getNombre_Equipo() {
        return nombre_Equipo;
    }

    public void setNombre_Equipo(String nombre_Equipo) {
        this.nombre_Equipo = nombre_Equipo;
    }

    public String getDueno_Equipo() {
        return dueno_Equipo;
    }

    public void setDueno_Equipo(String dueno_Equipo) {
        this.dueno_Equipo = dueno_Equipo;
    }

    public String getPais() {
        return pais;
    }

    public void setPais(String pais) {
        this.pais = pais;
    }

    public String getLiga_inscrita() {
        return liga_inscrita;
    }

    public void setLiga_inscrita(String liga_inscrita) {
        this.liga_inscrita = liga_inscrita;
    }
    
    
    
}
